package com.example.alessandrovangeli.chat_client;

import android.support.annotation.MainThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class ChatActivity extends AppCompatActivity {

    private Socket socket;

    private EditText inputMessage;
    private TextView response;
    private Button invia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        inputMessage = (EditText) findViewById(R.id.user_input);
        response = (TextView) findViewById(R.id.messageResponse);

        try {
            socket = IO.socket("http://172.31.98.221:3000");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        socket.on("new message", onNewMessage);
        socket.connect();



        invia = (Button) findViewById(R.id.invia);
        invia.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });


    }

    private void sendMessage() {

        String messaggio = inputMessage.getText().toString();

        socket.emit("new message", messaggio);
        inputMessage.setText("");
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable(){

                @Override
                public void run() {
                    String messaggio = args[0].toString();
                    response.append(messaggio+"\n");

                }

            });

        }
    };
}
